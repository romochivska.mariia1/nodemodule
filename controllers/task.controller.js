
const Square = require('../models/calculations');

exports.calculateSquare = async (req, res) => {
  try {
    const { length } = req.body;

   
    if (length < 0) {
      return res.status(400).json({ error: 'Length cannot be negative' });
    }

    const area = length ** 2;
    const perimeter = 4 * length;

    const square = new Square({ length, area, perimeter });
    await square.save();

    res.json({ area, perimeter });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

