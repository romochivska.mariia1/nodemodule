const express = require('express');
const mongoose = require('mongoose');
const app = express();
const squareRouter = require('./routes/task.route');

const PORT = process.env.PORT || 3000;


mongoose.connect('mongodb://localhost:27017/ModuleNode', { useNewUrlParser: true, useUnifiedTopology: true });


app.use(express.json());


app.use('/square', squareRouter);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});