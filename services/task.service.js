const Calculation = require('../models/сalculation');
const createError = require('http-errors');

exports.calculateSquare = async (req, res, next) => {
  try {
    const length = parseFloat(req.body.length);
    if (!isNaN(length) && length > 0) {
      const area = length ** 2;
      const perimeter = 4 * length;
      const calculation = new Calculation({ length, area, perimeter });
      await calculation.save();
      res.json({ area, perimeter });
    } else {
      throw createError.BadRequest('Invalid input');
    }
  } catch (err) {
    next(err);
  }
};

exports.checkRequestBody = async (req, res, next) => {
  try {
    if (!req.body || Object.keys(req.body).length === 0) {
      throw createError.BadRequest('Request body is empty or missing');
    }
    next();
  } catch (err) {
    next(err);
  }
};
