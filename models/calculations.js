const mongoose = require('mongoose');

const calculationSchema = new mongoose.Schema({
  length: Number,
  area: Number,
  perimeter: Number,
  timestamp: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Calculation', calculationSchema);