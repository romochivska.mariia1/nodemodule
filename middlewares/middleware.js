
exports.checkInput = (req, res, next) => {
    const { length } = req.body;
  
    // Перевірка чи довжина сторони є числом
    if (typeof length !== 'number') {
      return res.status(400).json({ error: 'Length must be a number' });
    }
  
    next();
  };
  