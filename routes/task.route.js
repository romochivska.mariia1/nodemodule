const express = require('express');
const router = express.Router();
const squareController = require('../controllers/task.controller');
const { checkInput } = require('../middlewares/middleware');

router.post('/calculateSquare', checkInput, squareController.calculateSquare);

module.exports = router;